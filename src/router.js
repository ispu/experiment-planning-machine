import Vue from 'vue'
import Router from 'vue-router'
import DefaultLayout from './layouts/Default.vue'
import Home from './views/Home.vue'
import GaussianElimination from './components/GaussianElimination.vue'
import LeastSquares from './components/LeastSquares.vue'
import GradientDescent from './components/GradientDescent.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: DefaultLayout,
      children: [
        {
          path: '',
          name: 'home',
          component: Home
        },
        {
          path: '/GaussianElimination',
          name: 'GaussianElimination',
          component: GaussianElimination
        },
        {
          path: '/LeastSquares',
          name: 'LeastSquares',
          component: LeastSquares
        },
        {
          path: '/GradientDescent',
          name: 'GradientDescent',
          component: GradientDescent
        }
      ]
    }
  ]
})
