const puppeteer = require('puppeteer');

(async () => {
  const durations = []
  const browser = await puppeteer.launch()
  for (let i = 0; i < 10; i++) {
    const page = await browser.newPage()
    await page.goto('http://localhost')
    const metrics = await page.metrics()
    durations.push(metrics.TaskDuration)
  }
  await browser.close()
  const min = Math.min(...durations)
  const max = Math.max(...durations)
  console.log('Min', min)
  console.log('Check', max - min)
})()
