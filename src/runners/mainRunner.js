const AMOUNT_OF_VARIABLES = 4
const factorMatrix = []
for (let i = 0; i < (1 << AMOUNT_OF_VARIABLES); i++) {
  let boolArr = []

  for (let j = AMOUNT_OF_VARIABLES - 1; j >= 0; j--) {
    boolArr.push(i & (1 << j) ? -1 : 1)
  }
  factorMatrix.push(boolArr)
}
console.log(factorMatrix)
